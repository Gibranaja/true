<?php

    require_once "../modals/fasilitasKamar.php";

    $fasilitaskamar = new FasilitasKamar();


    // cek jika ada id_kamar, tipekamar, fasilitas_kamar pada request
    // jika ada id kamar maka cleanstring
    // jika tidk ada maka kosongkan
    $id_kamar = isset($_POST["id_kamar"]) ? cleanString($_POST["id_kamar"]): "";
    $tipe_kamar = isset($_POST["tipe_kamar"]) ? cleanString($_POST["tipe_kamar"]): "";
    $fasilitas_kamar = isset($_POST["fasilitas_kamar"]) ? cleanString($_POST["fasilitas_kamar"]): "";

    // struktur kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_kamar)){
                // jika $id-kamar tidak ada pada request, jalankan method insert
                $response = $fasilitaskamar->insert($tipe_kamar, $fasilitas_kamar);
            }else{
                // jika $id_kamar ada, jalankan method edit
                $response = $fasilitaskamar->update($id_kamar, $tipe_kamar, $fasilitas_kamar);
            }
            break;

            case 'get_data' :
                $response = $fasilitaskamar->get_data();

                $data = Array();

                while($row = $response->fetch_object()){
                    $data[] = array(
                        "0"=>$row->tipe_kamar,
                        "1"=>$row->fasilitas_kamar,
                        "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_kamar.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-info btn-sm" onclick="delete_data('.$row->id_kamar.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                    );
                }
            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data
            );
            echo json_encode($result);
            break;

            case 'show' :
                $response = $fasilitaskamar->show($id_kamar);
                echo json_encode($response);
            break;

            case 'delete_data' :
                $response = $fasilitaskamar->delete_data($id_kamar);
            break;
            
    }