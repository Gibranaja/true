<?php

    if(isset($_POST["submit"]) and $_POST["submit"]=="login"){

        require_once("../modals/User.php");
        $user = new User();

        $username = $_POST['username'];
        $pass = $_POST['pass'];

        if(empty($username) AND empty($pass)){
            header("Location:".BASE_URL."view/loginResepsionis.php?m=2");
            exit();
        }else{
            $result = $user->login($username, $pass);
            $get = $result->fetch_object();
            session_start();
            if(isset($get)){
                $_SESSION["id_user"]=$get->id_user;
                $_SESSION["pass"]=$get->pass;
                header("location:".BASE_URL."view/dashboard");
                exit();
            }else{
                header("Location:".BASE_URL."view/loginResepsionis.php?m=1");
                exit();
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application</title>

	<link href="../public/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="../public/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="../public/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="../public/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="../public/img/favicon.png" rel="icon" type="image/png">
	<link href="../public/img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../public/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="../public/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../public/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../public/css/main.css">
</head>
<body>

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                
                <form class="sign-box" action="" method="POST" id="login_form">
                    <div class="sign-avatar">
                        <img src="../public/1.png" alt="" id="img-icon">
                    </div>
                    <input type="hidden" id="role_id" name="role_id" value="1">
                    <header class="sign-title" id="lblHeader">Akses Resepsionis</header>
                    <!-- CHECK CONDITION FOR ALERT -->
                    <?php
                    require_once 'layoutPartial/alert.php';
                    ?>
                    
                        <!-- END CHECK CONDITIONFOR ALERT -->

                    <div class="form-group">
                        <input type="text"  name="username" class="form-control" placeholder="Username"/>
                    </div>
                    <div class="form-group">
                        <input type="password"  name="pass" class="form-control" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                        
                        <div class="float-right reset">
                            <a href="../index.php">Home</a>
                        </div>
                        <div class="float-left reset">
                            <a href="loginAdmin.php" id="btnAkses">Akses Admin</a>
                        </div>
                    </div>
                    <input type="hidden" name="submit" value="login">
                    <button type="submit" class="btn btn-rounded">Sign in</button>
                   
                </form>
            </div>
        </div>
    </div><!--.page-center-->


<script src="../public/js/lib/jquery/jquery.min.js"></script>
<script src="../public/js/lib/tether/tether.min.js"></script>
<script src="../public/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="../public/js/plugins.js"></script>
    <script type="text/javascript" src="../public/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="../public/js/app.js"></script>
<script src="index.js" type="text/javascript"></script>
</body>
</html>