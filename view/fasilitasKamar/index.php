<?php
    require_once '../../config/connection.php';
    session_start();
    if(isset($_SESSION["role"])){

    
?>
<?php
    require_once("../layoutPartial/head_template.php");
?>
<body class="with-side-menu">

<?php
    require_once("../layoutPartial/header.php");
?>
	

	<div class="mobile-menu-left-overlay"></div>
<?php
    require_once("../layoutPartial/nav.php");
?>
	

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Fasilitas Kamar</h2>
                            
							
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block" id="daftarkamar">
                    <button type="button" class="btn btn-inline btn-success" title="Tambah Data Kamar" id="btnTambah" onclick="showForm(true)">+</button>
					<table id="tbl_list" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Tipe Kamar</th>
							<th>Fasilitas Kamar</th>
							<th>Aksi</th></th>
						</tr>
						</thead>
						<tbody>		
						</tbody>
					</table>
				</div>
			</section>
            <section class="card">
            <form id="formTambah" method="POST">
                    <div class="box-typical box-typical-padding">

                    <input type="hidden" name="id_kamar" id="id_kamar">
		
                        <h5 class="m-t-lg with-border">Tambah Fasilitas Kamar</h5>

                        <div class="row">
                            <div class="col-lg-6">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Tambah Fasilitas Kamar</label>
                                    <input type="text" class="form-control" id="tipe_kamar" name="tipe_kamar" placeholder="">
                                    
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Fasilitas Kamar</label>
                                    <input type="text" class="form-control" id="fasilitas_kamar" name="fasilitas_kamar" placeholder="">
                                    
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-inline btn-success" id="btnSimpan">Simpan</button>
                                <button type="button" class="btn btn-inline btn-danger" onclick="closeForm()">Batal</button>
                            </div>
                            
                        </div><!--.row-->
			        </div><!--.box-typical-->
                </form>
            </section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php
    require_once("../layoutPartial/script_template.php");
?>

<script type="text/javascript" src="fasilitasKamar.js"></script>
	
</body>
</html>

<?php
    }else{
        header("Location:".BASE_URL);
    }
?>