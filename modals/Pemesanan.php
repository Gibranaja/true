<?php

    require "config/connection.php";

    Class Pemesanan
    {
        public function __construct()
        {}

        public function insert($nm_pemesanan, $email, $no_hp, $nm_tamu, $id_kamar, $cek_in, $cek_out, $jml)
        {
            $sql = "INSERT INTO tb_pemesanan(nm_pemesanan, email, no_hp, nm_tamu, id_kamar, cek_in, cek_out, jml)
            VALUES
            ('$nm_pemesanan','$email','$no_hp','$nm_tamu','$id_kamar','$cek_in','$cek_out','$jml')";
            return runQuery($sql);
        }
        public function get_data()
        {
            $sql = "SELECT tb_pemesanan.id_pemesanan, 
            tb_pemesanan.nm_pemesanan, 
            tb_pemesanan.email, 
            tb_pemesanan.no_hp, 
            tb_pemesanan.nm_tamu, 
            tb_pemesanan.id_kamar, 
            tb_pemesanan.cek_in, 
            tb_pemesanan.cek_out, 
            tb_pemesanan.jml, 
            tb_kamar.tipe_kamar 
            FROM tb_pemesanan INNER JOIN tb_kamar ON tb_pemesanan.id_kamar = tb_kamar.id_kamar;";
            return runQuery($sql);
        }
    }